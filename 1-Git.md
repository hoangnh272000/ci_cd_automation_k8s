# Git

## VCS - Hệ thống quản lý phiên bản

Version Control System (VCS) là 1 hệ thống ghi nhận và lưu lại sự thay đổi của các file theo thời gian, từ hệ thống đó 1 file có thể phục hồi quay về trạng thái (version) ở 1 thời điểm trước đó. Ngoài ra có thể theo dõi sự thay đổi của file theo thời gian, ai đã thay đổi, thay đổi vào lúc nào, ...

DVCS - Distributed Version Control System - Hệ thống quản lý phiên bản phân tán : Có 1 server ở đó có Database lưu giữ các version của file, các developer kết nối vào server không chỉ lấy file mà có lấy luôn cả hệ thống Database. Tức là khi server bị ngắt thì các develop vẫn làm việc bình thường trên Database ở local, sau đó commit lên Server sau, hoặc Database ở Server bị lỗi thì developer bất kỳ có thể khôi phục lại cho server.

<img src="./Images/DVCS.png"/>

## Git

Chính là hệ thống quản lý phiên bản phân tán (DVCS), với các ưu điểm: tốc độ, đơn giản, phân tán, phù hợp với dự án lớn nhỏ.

### Cách lưu dữ liệu của Git

Git lưu dữ liệu dưới dạng 1 loạt snapshot của 1 tập hợp các file, có nghĩa là mỗi khi `commit` thì Git sẽ tiến hành snapshot lại hệ thống các file thời điểm đó và lưu giữ 1 tham chiếu tới snapshot đó, nếu các file không có thay đổi thì Git sẽ không lưu lại file đó lần nữa mà chỉ có 1 liên kết đến file đã lưu ở trước.

### Lưu dữ liệu trong Git đảm bảo tính toàn vẹn

Mọi thứ trước khi được lưu trữ vào Git đều được kiểm tra bởi mã băm (hash, checksum), có nghĩa là không thể thay đổi nội dung file mà Git không biết về sự thay đổi đó. Chức năng này giúp cho bạn không thể mất thông tin khi trao đổi dữ liệu hay file lỗi mà không thể nhận ra được. Git sử dũng mã hash SHA-1, mỗi chuỗi hash SHA-1 sinh ra căn cứ theo nội dung của file dài 40 ký tự

### 3 state của Git

Với Git, các file của bạn có thể nằm ở một trong ba trạng thái: `committed`, `modified`, `staged`
- **Committed** : dữ liệu đã lưu trữ an toàn trong Database (local)
- **Modified** : dữ liệu (file) có thay đổi nhưng chưa lưu trong Database (local)
- **Staged** :  đánh dấu các file sửa đổi modified chuẩn bị được commit tiếp theo

Các khái niệm trên dẫn tới Git tổ chức một dự án ra ba khu vực:
1. Thư mục làm việc (Working tree) - Chứa bản sao phiên bản cụ thể của dự án
2. Khu vực sắp xếp (staging) - là một file, nằm trong thư mục Git chứa thông tin sẽ commit
3. Thư mục Git (Git directory) - nơi Git lưu Database các file Metadata

<img src="./Images/state-git.png"/>

- Bạn code, sửa đổi các file trong (1) Working tree
- Bạn tổ chức những sự thay đổi nào muốn commit tiếp theo, cơ bản là việc đánh dấu sự thay đổi vào staging area (2)
- Bạn thực hiện commit để các file đánh dấu trong staging area và lưu vào (3) Git directory như nhưng snapshot (chụp file)

### Cấu hình Git

Các tham số chung để bạn làm việc với Git được thiết lập với lệnh `git config` với vài cách khác nhau, nó thiết lập các biến tham số, các biến này có thể lưu trữ ở một số nơi tùy cách bạn thiết lập như:
- `/etc/gitconfig` : tham số khai báo trong file này có hiệu lực cho mọi user, dự án. Lệnh tương tác thiết lập với tham số `--system`
- `~/gitconfig` hoặc `~/.config/git/config`: thiết lập có hiệu lực với User hiện tại của hệ thống, thực hiện với tham số `--global`
- `.git/config` : cấu hình lưu ở chính thư mục .Git của dự án (repository), chỉ có tác dụng cho Repository này.

## Git init - Khởi tạo

Sử dụng lệnh git init để khởi tạo một local repo mới hoặc repo lưu trữ ở server với git init --bare

### Command git init

Lệnh **git init** được sử dưng để tạo, khởi tạo một kho chứa Git mới (Git Repo) ở local. Khi đang trong thư mục dự án chạy lệnh `git init` nó sẽ tạo ra một thư mục con (ẩn) tên .git, thư mục này chứa tất cả thông tin mô tả cho kho chứa dự án (Repo) mới - những thông tin này gọi là metadata gồm các thư mục như objects, refs ... Có một file tên `HEAD` cũng được tạo ra - nó trỏ đến `commit` hiện tại.

Lệnh `git init` nhanh chóng tạo ra quản lý phiên bản của dự án dạng none bare mà bạn không cần có ngay một server để lưu Repo từ xa, không yêu cầu bạn phải nạp file dữ liệu nào. Tất cả phải làm là vào thư mục dự án cần khởi tạo và thi hành lệnh sau để khởi tạo:
```
git init
```

### Command girt init --bare

Khi bạn cần tạo ra một Repo Git mà nó chỉ có chức năng lưu trữ - không có thư mục làm việc thì thực hiện lệnh:
```
git init --bare
```
Loại dự án Git này thì bạn có thể truy cập, lưu trữ, nhưng không soạn thảo, sửa file, thực hiện commit trực tiếp tại dự án. Thường tạo loại dự án này để lưu trữ như là Remote Repo (Tạo Repo git trên Server), từ đó lấy về Local (lệnh git clone), và để local đẩy dữ liệu Git lên

<img src="./Images/bare-repo.png"/>

### Git add - chuẩn bị commit

Thực hiện lệnh tạo snapshot, lưu thông tin thay đổi vào vùng staging để chuẩn bị cho lệnh commit

Lệnh `git add` sử dụng để đánh chỉ mục (index) các nội dung mới, mới cập nhật trong `Working Directory`, nó chuẩn bị nội dung sắp xếp cho lần commit tiếp theo.

Khái niệm index ở trên có nghĩa là lưu lại snapshot thông tin thay đổi của `Working Directory` so với lần commit trước (hoặc so với snapshot chưa commit), snapshot lưu ở `Staging Area`.

Bạn có thể thực hiện lệnh `git add` nhiều lần để tạo tạo ra một snapshot cuối cùng trước khi thực hiện commit

<img src="./Images/git-add.png"/>

### Git status - Xem trạng thái của Repo

Sử dụng lệnh git để biết thông tin trạng thái sửa đổi, thêm mới, xóa các file trước khi thực hiện commit

Lệnh git status hiện thị thông tin khác nhau (do thêm mới, xóa đi, sửa đổi các file) giữa các file trong các trường hợp:
1. Khác nhau giữa các file trong staging area (chỉ mục) và commit tại con trỏ HEAD (Thường HEAD ở vị trí commit cuối): thông tin này bạn có thể thực hiện lệnh commit để lưu staging vào dữ liệu Git
2. Khác nhau giữa các file trong working directory và trong staging area: bạn có thể chạy git add rồi commit
3. Khác nhau giữa working directory và những file chưa được giám sát bởi Git: bạn có thể chạy git add rồi commit

Thông thường thì có thể thi hành ngay lệnh với cú pháp đơn giản sau để có thông tin trạng thái đầy đủ, chỉ tiết
```
git status
```
Nếu muốn hiện thị thông tin ngắn gọn hơn thì cho thêm tham số `-s`
```
git status -s
```

Lúc này trước các file có sự thay đổi có thể có các ký tự tương ứng với các thông tin gồm:
- ' ' = unmodified (không đổi)
- M = modified (có sửa đổi)
- A = added (file mới thêm)
- D = deleted (file bị xóa)
- R = renamed (đổi tên file)
- C = copied (file copy từ file khác)
- U = updated but unmerged (đã cập nhật, nhưng chưa merge)

### Git commit

Lệnh git commit thực hiện lưu vào CSDL Git toàn bộ nội dung chứa trong index (staging area) và kèm theo nó là một đoạn text thông tin (log) mô tả sự thay đổi của của commit này so với commit trước. Sau khi commit con trỏ HEAD tự động dịch chuyển đến commit này (Trong branch hiện tại)

*Khi thực hiện commit nếu bạn nhận ra ngay có sự nhầm lẫn nào đó bạn có thể khôi phục lại trạng thái cũ bằng lệnh `git reset`*

Lệnh commit cơ bản, đơn giản nhất là thực hiện với tham số -m để kèm dòng thông tin về commit
```
git commit -m "Ghi chú về commit"
```
Lệnh trên tạo ra một commit với nội dung lấy từ staging area, một điểm trong lịch sử commit được tạo ra với thông tin là dòng thông tin nhập vào, sau này bạn có thể xem lại lịch sử này bằng lệnh `git log`

Nếu commit đã được tạo ra nhưng chưa thực hiện push lên remote (khi có làm việc với Remote Repo - nói ở các phần sau) thì bạn có thể tạo ra commit mới thay thế cho commit cuối cùng đó. Dùng trong trường hợp không muốn tạo ra nhiều commit trong lịch sử commit thì cho vào lệnh tham số `--amend`
```
git commit --amend -m "Thông tin thay đổi"
```

### Git reset

Khi đã thực hiện commit, commit đó chưa public (chưa đẩy lên Remote Repo bằng lệnh git push) thì bạn có thể hủy (undo) commit đó với hai trường hợp bằng lệnh `git reset` như sau
```
git reset --soft HEAD~1
```

<img src="./Images/git-reset.png"/>

Khi dùng tham số `--hard` thì kết quả giống với dùng tham số `--soft`, chỉ có một khác biết là nội dung thay đổi của commit cuối không đưa đưa vào staging area mà bị hủy luôn. Trường hợp này dùng khi bạn quyết định hủy hoàn toàn commit cuối
```
git reset --hard HEAD~1
```

<img src="./Images/git-reset-2.png"/>

### Git log

Lệnh `git log` giúp bạn xem lại thông tin lịch sự commit, nhằm giám sát sự thay đổi của dự án. Lệnh `git log` có nhiều tham số để xuất ra, định dạng các thông tin hiện thị theo cách mong muốn. Bạn có thể định dạng cách các thông tin mỗi commit được in ra khi xem, cũng như có thể lọc thông tin nào đó.
```
git log
```

Khi số lượng log nhiều, nó hiện thị trước một trang log, sau đó có dấu nhắc chờ lệnh để bạn điều hướng, tìm kiếm ... Để có trợ giúp về các lệnh này hãy nhấn h tại dấu nhắc lệnh

Một số phím chức năng bạn có thể nhập đề điều hướng và tìm kiếm trong log như:
- `return` - dòng tiếp theo
- `w` - trang tiếp
- `spacebar` - trang trước
- `q` - thoát
- `?pattern` - tìm kiếm, với pattern là mẫu tìm kiếm (keyword)
- `/pattern` - giống ?pattern
- `n` - đến vị trí tìm kiếm phía dưới
- `N` - đến kết quả tìm kiếm phía trước

Lọc theo ngày bạn có thể dùng tham số `--after="year-month-day"` hoặc `--before="year-month-day"` hoặc dùng cả hai để chỉ ra khoảng ngày. Ví dụ: hiện thị các log từ ngày 1/1/2022 đến ngày 31/12/2022
```
git log --after="2022-1-1" --before="2022-12-31"
```

### Git diff

Lệnh `git diff` hiện thị thông tin thay đổi giữa working directory và vùng index (staging area) hoặc với commit cũ, thông tin thay đổi giữa index(staging area) và commit, thông tin thay đổi giữa hai branch ...
```
git diff
```
Nó hiện thị thông tin tùy ngữ cảnh như sau:
- Thông tin khác nhau giữa working directory và commit cuối khi mà vùng index (staging area) không có dữ liệu gì
- Thông tin thay đổi giữa index và commit cuối nếu vùng index có dữ liệu

### Git clone

Lệnh `git clone` để sao chep, copy một Git Repo (kho chứa dự án Git) về máy đang local. Một số trường hợp sử dụng `git clone` như:
- Copy một Repo từ máy Remote về Local
- Copy một Repo từ thư mục này sang một thư mục khác
- Copy một Repo từ một Url (https) ví dụ GitLab

Trên máy của bạn có một Git Repo ở đường dẫn path-git, bạn có thể copy sang vị trí khác bằng lệnh
```
git clone 'path-git'
```

Nhiều dịch vụ Git cung cấp kết nối bằng giao thức (https) ví dụ GitHub, GitLab thì copy về bằng lệnh:
```
git clone 'url-repo'
```

Mặc định nó sẽ sao chép về branch hoạt động, để xem tất cả các branch có trên Remote dùng lệnh
```
git branch --remote
```

### Git checkout - switch, restore

Lệnh `git checkout` được dùng để chuyển branch hoặc để phục hồi file trong working directory từ một commit trước đây ...

1. Chuyển branch

Giả sử đang ở branch nào đó, muốn chuyển sang branch `master` thì dùng lệnh:
```
git checkout master
```

### Branch và con trỏ HEAD

Trong Git, nhánh branch là hướng rẽ phát triển code, mới mục đích không làm rối hướng phát triển chính, sau đó một branch có thể tích hợp nhập vào branch chính. Nói chung trong Git luôn làm việc với các branch, mặc định bao giờ cũng có một branch chính tên là `master`

Tạo 1 branch mới:
```
git branch hoang
```

Chuyển qua branch mới
```
git checkout hoang
```

<img src="./Images/git-branch.png"/>

<img src="./Images/git-branch-2.png"/>

Quay trở lại branch master và commit.

<img src="./Images/git-branch-3.png"/>

Tạo 1 branch mới để sửa lỗi từ branch master.

<img src="./Images/git-branch-4.png"/>

### Branch Merge

<img src="./Images/git-branch-5.png"/>

Khi đã hoàn thành nhiệm vụ trên branch sualoigap nếu muốn các kết quả của branch này tích hợp thay đổi vào master thì tiến hành gộp 2 branch lại gọi là merge. Để làm việc đó cần chuyển về làm việc trên branch master bằng lệnh checkout rồi cho branch sualoigap gộp vào master bằng lệnh merge:
```
git checkout master
git merge sualoigap
```

### Xóa branch

```
git branch -d sualoigap
```

### Xử lý xung đột khi merge branch

Bây giờ giả sử có nhu cầu gộp code tại branch alpha vào branch master, trường hợp này không đơn giản như cách gộp branch sualoigap ở trên. Do cả 2 branch có nhiều commit kể từ thời điểm rẽ branch nên khi gộp nó sẽ xem xét sự thay đổi trên cả 2 branch tại ba điểm (three-way), thời điểm commit cuối của các branch và thời điểm rẽ branch, đó là các commit với snapshot C2, C4, C7 như hình dưới:

<img src="./Images/git-branch-6.png"/>