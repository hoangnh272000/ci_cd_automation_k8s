# CI/CD

Với mỗi phương pháp phát triển phần mềm continuous, bạn continuously build, test, deploy code lặp đi lặp lại. Quá trình lặp đi lặp lại này giúp giảm khả năng phát triển code mới dựa trên các version trước bị lỗi hoặc không thành công. Với phương pháp này, bạn cố gắng ít có sự can thiệp của con người hơn hoặc thậm chí không có sự can thiệp nào, từ lúc devolopment code mới đến lúc deployment.

3 cách tiếp cận chính cho phương pháp continuous là:
- Continuous Integration (Tích hợp liên tục)
- Continuous Delivery 
- Continuous Deployment (Triển khai liên tục)

## Continuous Integration

Xem xét 1 ứng dụng có code của nó được lưu trữ trong 1 Git repository trong GitLab. Dev push code thay đổi hằng ngày, nhiều lần 1 ngày. Cho mọi lần push lên repository, bạn có thể tạo 1 bộ scripts để build và test tự động cho ứng dụng của bạn. Những script này giúp giảm khả năng bạn đưa ra lỗi trong ứng dụng của mình.

Cái này được gọi là `Continuous Integration`. Mỗi thay đổi được gửi đến 1 ứng dụng, thập chí branch dev, nó build và test tự động và continuously. Các test này đảm bảo vượt qua các test, guidelines, các tiêu chuẩn code mà bạn thiết lập cho ứng dụng của mình.

Bản thân GitLab là 1 ví dụ cho dự án sử dụng `Continuous Integration` như 1 phương pháp phát triển phần mềm. Mỗi lần push đến project, bộ check sẽ chạy dựa trên code.

## Continuous Delivery

`Continuous Delivery` là bước xa hơn Continuous Integration. ứng dụng của bạn không chỉ được built và test mỗi khi code thay đổi được push lên codebase, ứng dụng cưỡng được deploy continuously. Tuy nhiên, với `Continuous Delivery`, bạn kích hoạt deployment theo cách thủ công.

`Continuous Delivery` sẽ check code tự động, nhưng nó đòi hỏi sự can thiệp của con người để kích hoạt việc deployment các thay đổi theo thủ công và chiến lược.

## Continuous Deployment

`Continuous Deployment` là bước xa hơn Continuous Integration, giống như Continuous Delivery. Sự khác biệt là thay vì deploy từng ứng cụng của bạn theo cách thủ công, bạn đặt nó deploy tự động. Sự can thiệp của con người là không cần thiết.

## GitLab CI/CD

GitLab CI/CD là 1 phần của GitLab mà bạn sử dụng cho tất cả phương thức Continuous (Continuous Integration, Delivery, Deployment). Với GitLab CI/CD, bạn có thể test, build, publish phần mềm của bạn mà không cần ứng dụng hoặc tích hợp của bên thứ 3.

### GitLab CI/CD Workflow

GitLab CI/CD phù hợp với workflow development :

Bạn có thể bắt đầu bằng cách thảo luận về việc triển khai code trong 1 issue và working locally về thay đổi được đề xuất của bạn. Sau khi bạn có thể push commit của mình vào 1 branch trong remote repository được lưu trữ trong GitLab. Push kích hoạt CI/CD pipeline cho project. Sau đó, GitLab CI/CD:
- Tự động chạy scripts để :
    - Build và Test ứng dụng
    - Xem trước các thay đổi trong Review App, giống như bạn sẽ thấy trên localhost

Sau khi triển khai hoạt động như mong đợi:
- Nhận code đã được review và approve
- Merge feature branch vào branch mặc định
    - GitLab CI/CD tự động deploy các thay đổi của bạn trong môi trường production.

Nếu gặp sự cố, bạn có thể roll back lại sự thay đổi của mình

<img src="./Images/ci-cd-workflow.png"/>

## GitLab workflow

Với GitFlow, dev tạo 1 branch develop và đặt nó làm mặc định, trong khi GitLab Flow hoạt động với branch 'main' ngay lập tức. GitLab Flow kết hợp 1 branch pre-production để fix bug trước khi merge thay đổi trở lại main trước khi chuyển sang production. Teams có thể add nhiều branch pre-production khi cần thiết - VD, Từ main đến test, từ test đến acceptance, từ acceptance đến production.

Team thực hành phân branch feature, trong khi vẫn duy trì 1 branch production. Bất cứ khi nào branch main sẵn sàng để deploy, users merge nó vào branch production và release. GitLab Flow thường sử dụng với các branch release. Các team yêu cầu public API có thể cần duy trì các version khác nhau. Sử dụng GitLab Flow, team có thể làm 1 branch v1 và 1 branch v2 duy trì riêng lẻ, điều này có thể hữu ích nếu team xác định lỗi trong quá trình review code quay trở lại v1.

### Lợi ích dùng GitLab Flow

GitLab Flow cung cấp 1 cách đơn giản, minh bạch và hiệu quả để làm việc với Git. Sử dụng GitLab Flow, dev có thể công tác và duy trì 1 số version phần mềm trong các môi trường khác nhau. GitLab Flow giảm chi phí releas, tagging, merging, đây là 1 thách thức phổ biến gặp phải với các loại Git workflow khác, để tạo ra 1 cách dễ dàng hơn để deploy code. Commit flow downstream để đảm bảo rằng mọi dùng code đều được kiểm tra trong mọi môi trường. Các team thuộc bất kỳ quy mô nào đều có thể sử dụng GitLab Flow và nó có tính linh hoạt để thích ứng với các nhu cầu và thách thức khác nhau.