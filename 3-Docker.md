# Docker

Docker là 1 nền tảng mở (open platform) cho developing, shipping, và running applications. Docker cho phép bạn tách các ứng dụng khỏi cơ sở hạ tầng để bạn có thể phân phối phần mềm 1 cách nhanh chóng. Với Docker, bạn có thể quản lý cơ sở hạ tầng của mình theo cách bạn quản lý các ứng dụng của mình. Bằng cách tận dụng các methodologies của Docker để shipping, testing và deploying code quickly, có thể giảm đáng kể độ trễ giữa việc viết code và chạy nó trong production.

# Docker platform

Docker cung cấp khả năng đóng gói và chạy 1 ứng dụng trong 1 môi trường isolated lỏng lẻo được gọi là 1 `container`. Isolation và bảo mật cho phép bạn chạy nhiều `container` đồng thời trên 1 host. `Container` nhẹ và chứa mọi thứ cần thiết để chạy ứng dụng, vì vậy không cần phải dựa vào những gì hiện đã được cài đặt trên host. Có thể dễ dàng chia sẻ `container` trong khi làm việc, và đảm bảo rằng mọi người mà bạn chia sẻ đều có cùng 1 `container` hoạt động theo đúng cách.

Docker cung cấp công cụ và 1 platform quản lý còng đời của `container`:
- Phát triển ứng dụng của bạn và các thành phần hỗ trợ của nó bằng cách sử dụng `containers`
- `Container` trở thành đơn vị phân phối và thử nghiệm ứng dụng của bạn
- Khi bạn sẵn sàng, triển khai ứng dụng vào môi trường production, như 1 `container` hoặc 1 orchestrated service. Điều này hoạt động giống nhau cho dù môi trường production là local data center, 1 cloud provider hoặc kết hợp cả 2.

# Dùng Docker để làm gì?

## Phân phối nhanh chóng, nhất quán các ứng dụng.

Docker hợp lý hóa vòng đời phát triển bằng cách cho phép các dev làm việc trong môi trường tiêu chuẩn hóa sử dụng `local containers` cung cấp các ứng dụng và dịch vụ của bạn. `Containers` rất tốt cho CI/CD workflow.

## Triển khai đáp ứng và mở rộng quy mô

Docker's container-based platform cho phép khối lượng công việc có tính di động cao. `Docker có thể chạy trên local laptop của dev, máy vật lý và VM trong 1 data center, trên cloud providers, hoặc nhiều môi trường khác nhau.`

Tính di động và nhẹ của Docker giúp dễ dàng quản lý động các khối lượng công việc, mở rộng hoặc thu nhỏ các ứng dụng và dịch vụ khi nhu cầu  kinh doanh yêu cầu, trong thời gian gần thực tế.

--> **Nhanh và có thể triển khai ở nhiều môi trường khác nhau : local, vm, cloud,...**

## Chạy nhiều khối lượng công việc hơn trên cùng 1 hardware

Docker nhẹ và nhanh. Nó cung cấp 1 giải pháp thay thế khả thi, hiệu quả về chi phí cho các VM dựa trên hypervison, để có thể sử dụng nhiều hơn khả năng tính toán của mình để đạt được các mục tiêu kinh doanh. Docker hoàn hảo cho môi trường mật độ cao và triển khai vừa và nhỏ, nơi bạn cần làm nhiều việc hơn với tài nguyên ít hơn.

--> **Có thể sử dụng nhiều Docker thay cho VM, VM tiêu tốn tài nguyên nhiều hơn trong khi Docker sử dụng ít hơn nhiều**

# Docker architecture

Docker sử dụng 1 client-server architecture. `Docker client` sẽ nói chuyện với `Docker daemon`, công đoạn này thực hiện công việc building, running và distributing (phân phối) Docker container. Docker client và daemon có thể chạy trên cùng 1 hệ thống, hoặc có thể kết nối 1 `Docker client` với 1 `Docker daemon` từ xa. Docker client và demon giao tiếp bằng cách sử dụng 1 REST API, qua UNIX sockets hoặc 1 network interface. 1 `Docker client` khác là `Docker compose`, cho phép bạn làm việc với các ứng dụng bao gồm 1 tập hợp các `container`.

<img src="./Images/docker-architecture.png"/>

## Docker daemon

Docker daemon (dockerd) lắng nghe requests Docker API và quản lý các đối tượng Docker như images, containers, networks và volumes. 1 daemon cũng có thể giao tiếp với các daemon khác để quản lý các service Docker.

## Docker client

Docker Client (docker) là cách chính mà nhiều Docker user tương tác với Docker. Khi sử dụng commands như `docker run`, client sẽ gửi những command đến `dockerd`, để thực hiện chúng. `docker command` sử dụng Docker API. Docker client có thể giao tiếp với nhiều hơn 1 daemon.

## Docker Desktop

Docker destop là ứng dụng cài đặt cho môi trường MAC, Windows hoặc Linux cho phép bạn build và share các ứng dụng và dịch vụ được chứa trong container. Docker desktop bao gồm Docker daemon(dockerd), Docker client (docker), Docker Compose, Docker Content Trust, k8s, Credential Helper.

## Docker registries

1 Docker registry stores Docker images. Docker Hub là 1 public registry mà nhiều người có thể sử dụng, và Docker được cấu hình để tìm kiếm images trên Docker Hub. Có thể chạy private registry riêng của bạn.

## Docker objects

Khi sử dụng Docker, bạn có thể tạo và sử dụng images, containers, networks, volumes, plugins, và các objects khác.
### Images

1 `image` là 1 mẫu read-only với các hướng dẫn để tạo `Docker container`. 1 `image` dựa trên 1 `image` khác với 1 số tùy chọn bổ sung. VD : bạn có thể build 1 `image` dựa trên 1 `image` ubuntu, nhưng cài đặt Apache web server và ứng dụng của bạn, cũng như các chi tiết cấu hình cần thiết để chạy ứng dụng của bạn.

Có thể tạo `images` riêng của mình hoặc có thể sử dụng của người khác publish lên registry. Để build `images` của riêng bạn, bạn tạo 1 `Dockerfile` với syntax đơn giản để xác định các bước cần thiết để build và chạy nó. Mỗi hướng dẫn trong Dockerfiile tạo ra 1 layer trong `image`. Khi bạn thay đổi `Dockerfile` và rebuild `image`, chỉ những layer nào thay đổi sẽ rebuild. Đó là 1 phần lý do làm cho `image` trở nên nhẹ, nhỏ và nhanh, khi so sáng với các công nghệ ảo khóa khác.

### Containers

1 `container` là 1 ví dụ có thể chạy được của image. Bạn có thể tạo, start, stop, move hoặc delete 1 `contaier` sử dụng Docker API hoặc CLI. Bạn có thể kết nối 1 `container` cho 1 hoặc nhiều network, gán storage vào nó, hoặc tạo 1 image mới dựa trên trạng thái hiện tại của nó.

1 `container` được cách lý tương đối tốt với các `container` khác và máy chủ của nó. Bạn có thể kiểm tra mức độ tách biệt giữa network, storage của `container` hoặc các hệ thống con cơ bản khác nhau với các `container` khác hoặc với host.

`Container` được xác định bởi image của nó cũng như bát kỳ tùy chọn cấu hình nào bạn cung cấp cho nó khi bạn tạo hoặc start nó. Khi 1 `container` bị xóa, mọi thay đổi về trạng thái của nó không được lưu trữ trong persistent storage sẽ biến mất.

# Dockerfile

Docker build images đọc hướng dẫn trong `Dockerfile`. Đây là 1 file text chứa các hướng dẫn tuân theo 1 định dạng cụ thể để tập hợp ứng dụng của bạn thành 1 container image. 
- **FROM <image>** : Xác định cơ sở cho image
- **RUN <command>** : Thực hiện bất cứ command nào trong 1 layer mưới trên đầu image hiện tại và commit kết quả. RUN cũng có 1 dạng shell để chạy các command.
- **WORKDIR <directiory>** : Đặt working directory cho bất kỳ lệnh RUN, CMD, ENTRYPOINT, COPY, ADD nào theo sau nó trong Dockerfile.
- **COPY <src> <dest>** : Cope các file hoặc directory mới từ <src> và thêm nó vào filesystem của container tại path <dest>.
- **CMD <command>** : Cho phép xác định chương trình mặc định được chạy khi bạn start container dựa trên image. Mỗi Dockerfile chỉ có 1 CMD và chỉ biên phải CMD cuối cùng được respect khi tồn tại nhiều CMD.

## Giảm số lượng Layer hình thành nên Image

**Cố gắng xây dựng image có ít layer nhất**

Trong Dockerfile có bao nhiêu chỉ thị RUN thì có bấy nhiêu layer được tạo ra, tương tự là ADD, ENTRYPOINT, CMD ... Nên muốn ít layer thì cần viết sao cho ít chỉ thị nhất

# Docker Compose

Compose là 1 tool để xác định và run các ứng dụng multi-contaier Docker. Với Compose, bạn sử dụng 1 file YAML để cấu hình các dịch vụ của ứng dụng. Sau đó, với 1 command duy nhất, bạn tạo và start tât cả dịch vụ từ cấu hình của mình.

Compose hoạt động ở tất cả các môi trường: production, staging, development, testing, cũng như CI workflows. 

Sử dụng Compose là 1 quy trình gồm 3 bước:
1. Xác định môi trường ứng dụng của bạn bằng Dockerfile để nó có thể sử dụng ở mọi nơi
2. Xác định các dịch vụ tạo nên ứng udngj của bạn trong docker-compose.yml để chúng có thể chạy cùng nhau trong 1 môi trường riêng biệt
3. Run `docker compose up` và Dock compose command start và chạy taonf bộ ứng dụng. Bạn có thể run `docker-compose up` sử dụng Compose standalone (`docker-compose` binary).

## File docker-compose.yml

File docker-compose.yml gần giống ý nghĩa với file Dockerfile, là 1 file text, viết với định dạng YAML là cấu hình để từ đó lệnh `docker-compose` sinh ra và quản lý các service (container), network, ổ đĩa,... cho 1 ứng dụng hoàn chỉnh.

## Lưu image ra file, Nạp image từ file

Nếu muốn chia copy image ra máy khác ngoài cách đưa lên repository có thể lưu ra file, lệnh sau lưu image có tên `myimage` ra file
```
docker save --output myimage.tag myimage
```

File này có thể lưu trữ, copy đến máy khác để nạp vào docker:
```
docler load -i myimage.tag
```

Đổi tên 1 image đang có:
```
docker tag image_id imagename:version
```

## Chia sẻ dữ liệu giữa Docker Host và container

<img src="./Images/mount-volume.png"/>

Máy host là hệ thống bạn đang chạy Docker Engine. Một thư mục của máy Host có thể chia sẻ để các Container đọc, lưu đữ liệu.

### Container - ánh xạ thư mục máy host

Thư mục cần chia sẻ dữ liệu trên máy host là : `path_in_host`
Khi chạy container thư mục đó được mount - ánh xạ tới `path_in_container` của container.
Để có kết quả đó, tạo - chạy container với tham số thêm vào `-v path_to_data:path_in_container`

## Network trong Docker, liên kết mạng trong container

Docker cho phép bạn tạo ra một network (giao tiếp mạng), sau đó các container kết nối vào network. Khi các container cùng một network thì chúng có thể liên lạc với nhau nhanh chóng qua tên của container và cổng (port) được lắng nghe của container trên mạng đó.

Các network được tạo ra theo một driver nào đó như bridge, none, overlay, macvlan. Trong phần này sẽ sử dụng đến bridge network: nó cho phép các container cùng network này liên lạc với nhau, cho phép gửi gói tin ra ngoài

### Cổng - Port

Các kết nối mạng thông qua các port, để thiết lập cấu hình các port của container chú ý: có port bên trong container, có port mở ra bên ngoài (public), có giao thức của cổng (tpc, udp). Khi chạy container (tạo) cần thiết lập port thì đưa vào lệnh docker run tham số dạng sau:
```
docker run -p public-port:target-port/protocol ... 
```
- `public-port` : port public ra ngoài (ví dụ : 80, 8080,...) các kết nối không cùng network đến container phải thông qua port này.
- `target-port` : port bên trong container, public-port sẽ ánh xạ vào port này. Nếu các container cùng network có thể kết nối với nhau thông qua port này.