# GitLab Runner

GitLab Runner là 1 ứng dụng hoạt động với GitLab CI/CD để chạy các job trong 1 pipeline.

Bạn có thể chọn install ứng dụng GitLab Runner trên cơ sở hạ tầng mà bạn sở hữu hoặc quản lý. Bạn nên install GitLab Runner trên 1 máy tách biệt với máy lưu trữ phiên bản GitLab vì lý do bảo mật và hiệu suất. Khi sử dụng các máy riêng biệt, bạn có thể có các hệ điều hành và công cụ khác nhau như K8s hoặc Docker trên mỗi máy.

GitLab Runner là open-source và viết bởi Go. Nó có thể chạy dưới dạng 1 tệp nhị phân duy nhất; Không yêu cầu ngôn ngữ cụ thể là cần thiết.

Bạn có thể install GitLab Runner trên 1 số hệ điều hành được hỗ trợ khác nhau. Cá hệ điều hành khác cũng có thể hoạt động, miễn là bạn có thể biên dịch mã nhị phân Go trên chúng.

GitLab Runner có thể chạy bên trong Docker container hoặc deploy vào 1 Kubernetes cluster.